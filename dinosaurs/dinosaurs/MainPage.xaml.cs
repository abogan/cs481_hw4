﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace dinosaurs
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    public class TwoImagesTwoTexts
    {
        public string Text1 { get; set; }
        
        public ImageSource Image1 { get; set; }

        public Button Button1 { get; set; }
    }
    public partial class MainPage : ContentPage
    {
        ObservableCollection<TwoImagesTwoTexts> listOfTextAndImages;

        async void Handle_TRexButton(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new T_Rex());
        }
        async void Handle_StegoButton(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new Stego());
        }
        async void Handle_DinosaurJrButton(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new DinosaurJr());
        }
        async void Handle_ContextMenuMoreButton (object sender, System.EventArgs e)
        {
            
            await DisplayAlert("About", "This is an application focusing on dinosaurs. One of these is not an actual dinosaur. ", "OK");
        }
        
        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            ListView1.IsRefreshing = false;
            
        }
        public MainPage()
        {
            InitializeComponent();
            listOfTextAndImages = new ObservableCollection<TwoImagesTwoTexts>();
            PopulateImagesAndText();
        }
        private void PopulateImagesAndText()
        {

            listOfTextAndImages.Add(new TwoImagesTwoTexts

            {
                Text1 = "T-Rex",
                Image1 = "t_rex.jpg",
            }) ;
            listOfTextAndImages.Add(new TwoImagesTwoTexts
            {
                
                Text1 = "Stegosaurus",
                Image1 = "stegosaurus.jpg",
            });
            listOfTextAndImages.Add(new TwoImagesTwoTexts
            {
                
                Text1 = "Dinosaur Jr",
                Image1 = "dinosaurjr.jfif",
            }); 
            ListView1.ItemsSource = listOfTextAndImages;
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
           // DisplayAlert("Alert ", "Name is " + name, "Ok");
        }
    }
}
